import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer[] array = {2, 1, 5, 1, 3, 2};
        Integer k = 3;
        Integer max = 0, sum = 0;

        List<Integer> convertToList = convertArrayToList(array);

        for (int i = 0; i<convertToList.size()-k; i++) {
            for (int j = i; j<i+k; j++) {
                sum += convertToList.get(j);
            }
            if (sum > max) {
                max = sum;
            }
            sum = 0;
        }
        System.out.println(max);

    }

    public static List<Integer> convertArrayToList(Integer[] arrays) {
        List<Integer> convertToList = new ArrayList<>();
        for (int i = 0; i<arrays.length; i++) {
            convertToList.add(arrays[i]);
        }
        return convertToList;
    }
}